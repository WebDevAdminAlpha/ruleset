package ruleset

import (
	"errors"
	"os"
	"reflect"
	"testing"
)

// setFeatureEnv is a helper to explicitly set the value feature ENV
func setFeatureEnv(value string) (string, error) {
	err := os.Setenv(EnvVarGitlabFeatures, value)
	if err != nil {
		return value, err
	}
	return value, nil
}

func errEqual(t *testing.T, got error, want error) {
	if got != nil && want != nil {
		// support comparing errors without unwrap
		if got.Error() == want.Error() {
			return
		}
		t.Fatalf("expected error: %v \ngot: %v", want, got)
	} else if got != nil {
		t.Fatal(got)
	}
}

func TestLoadRuleset(t *testing.T) {
	var tests = []struct {
		enableCustomRulesets bool
		description          string
		rulePath             string
		analyzer             string
		wantErr              error
		wantRule             *Config
	}{
		{
			description:          "valid gosec analyzer ruleset",
			rulePath:             "testdata/sast-ruleset.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantRule: &Config{
				PassThrough: []PassThrough{
					{
						Type:  PassThroughFile,
						Value: "gosec-sast-config.json",
					},
				},
			},
		},
		{
			description:          "invalid analyzer",
			rulePath:             "testdata/sast-ruleset.toml",
			analyzer:             "gobadsec",
			enableCustomRulesets: true,
			wantErr: &ConfigNotFoundError{
				Analyzer:    "gobadsec",
				RulesetPath: "testdata/sast-ruleset.toml",
			},
		},
		{
			description:          "invalid rulePath",
			rulePath:             "testdata/sast-no-rules-here.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantErr: &ConfigFileNotFoundError{
				RulesetPath: "testdata/sast-no-rules-here.toml",
			},
		},
		{
			description:          "valid spotbugs analyzer ruleset",
			rulePath:             "testdata/sast-ruleset.toml",
			analyzer:             "spotbugs",
			enableCustomRulesets: true,
			wantRule: &Config{
				PassThrough: []PassThrough{
					{
						Type:   PassThroughRaw,
						Target: "settings.xml",
						Value:  "spotbugs setting value",
					},
					{
						Type:   PassThroughRaw,
						Target: "filter.xml",
						Value:  "spotbugs filter value",
					},
				},
			},
		},
		{
			description:          "valid gosec analyzer ruleset but disabled",
			rulePath:             "testdata/sast-ruleset.toml",
			analyzer:             "gosec",
			enableCustomRulesets: false,
			wantErr:              &NotEnabledError{},
		},
		{
			description:          "misconfigured toml",
			rulePath:             "testdata/misconfigured.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantErr: &InvalidConfig{
				RulesetPath: "testdata/misconfigured.toml",
				Err:         errors.New("(1, 2): unexpected token table key cannot contain ']', was expecting a table key"),
			},
		},
		{
			description:          "valid gosec analyzer ruleset with disabled rulesets",
			rulePath:             "testdata/sast-ruleset-disable-ids.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantRule: &Config{
				Ruleset: []Ruleset{
					{
						Identifier: Identifier{
							Type:  "CWE",
							Value: "CWE-1",
						},
						Disable: true,
					},
					{
						Identifier: Identifier{
							Type:  "CWE",
							Value: "Gosec Rule ID G203",
						},
						Disable: true,
					},
					{
						Identifier: Identifier{
							Type:  "CWE",
							Value: "Gosec Rule ID G204",
						},
						Disable: false,
					},
				},
			},
		},
		{
			description:          "valid gosec analyzer ruleset with invalid disabled flag scope",
			rulePath:             "testdata/sast-ruleset-disable-ids-with-bad-disable-scope.toml",
			analyzer:             "gosec",
			enableCustomRulesets: true,
			wantRule: &Config{
				Ruleset: []Ruleset{
					{
						Identifier: Identifier{
							Type:  "CWE",
							Value: "CWE-1",
						},
					},
					{
						Identifier: Identifier{
							Type:  "CWE",
							Value: "Gosec Rule ID G203",
						},
					},
				},
			},
		},
	}

	// Disable custom ruleset feature availability
	oldval, err := setFeatureEnv("")
	if err != nil {
		t.Fatal(err)
	}
	defer setFeatureEnv(oldval)

	for _, test := range tests {
		if test.enableCustomRulesets {
			setFeatureEnv(GitlabFeatureCustomRulesetsSAST)
		}

		rule, err := Load(test.rulePath, test.analyzer)
		errEqual(t, err, test.wantErr)

		if !reflect.DeepEqual(rule, test.wantRule) {
			t.Errorf("Wrong result. Expecting rule:\n%v\nGot:\n%v", test.wantRule, rule)
		}

		// clear feature
		setFeatureEnv("")
	}
}

func TestLoadRulesetFeatureUnsupported(t *testing.T) {
	// Disable custom ruleset feature availability
	oldval, err := setFeatureEnv("")
	if err != nil {
		t.Fatal(err)
	}
	defer setFeatureEnv(oldval)

	rule, err := Load("testdata/sast-ruleset.toml", "gosec")
	if err == nil {
		t.Fatal("Load should have returned an err")
	}
	if err.Error() != "custom rulesets not enabled" {
		t.Fatalf("expected error: %v \ngot: %v", "custom rulesets not enabled", err)
	}

	if rule != nil {
		t.Errorf("Wrong result. Expecting nil for rule.\nGot:\n%v", rule)
	}
}

func TestCustomRulesetEnabled(t *testing.T) {
	tests := []struct {
		enabled     bool
		rulesetPath string
		want        bool
	}{
		{
			enabled:     true,
			rulesetPath: "testdata/sast-ruleset.toml",
			want:        true,
		},
		{
			enabled:     false,
			rulesetPath: "testdata/sast-ruleset.toml",
			want:        false,
		},
	}

	// Disable custom ruleset feature availability
	oldval, err := setFeatureEnv("")
	if err != nil {
		t.Fatal(err)
	}
	defer setFeatureEnv(oldval)

	for _, test := range tests {
		if test.enabled {
			setFeatureEnv(GitlabFeatureCustomRulesetsSAST)
		}
		if got := customRulesetEnabled(); got != test.want {
			t.Errorf("Wrong result. Expecting %v for custRulesetEnabled. Got:%v", test.want, got)
		}
		// clear feature
		setFeatureEnv("")
	}
}

func TestDisabledIdentifiers(t *testing.T) {
	var tests = []struct {
		enabled         bool
		description     string
		rulePath        string
		wantErr         error
		analyzer        string
		wantDisabledIDs map[string]bool
	}{
		{
			enabled:         true,
			analyzer:        "gosec",
			description:     "valid gosec analyzer ruleset",
			rulePath:        "testdata/sast-ruleset-disable-ids.toml",
			wantDisabledIDs: map[string]bool{"CWE-1": true, "Gosec Rule ID G203": true},
		},
		{
			enabled:     true,
			analyzer:    "gosec",
			description: "valid gosec analyzer ruleset",
			rulePath:    "testdata/sast-ruleset-bad-path-disable-ids.toml",
			wantErr: &ConfigFileNotFoundError{
				RulesetPath: "testdata/sast-ruleset-bad-path-disable-ids.toml",
			},
			wantDisabledIDs: map[string]bool{},
		},
		{
			enabled:     true,
			analyzer:    "gobadsec",
			description: "valid gosec analyzer ruleset",
			rulePath:    "testdata/sast-ruleset-disable-ids.toml",
			wantErr: &ConfigNotFoundError{
				Analyzer:    "gobadsec",
				RulesetPath: "testdata/sast-ruleset-disable-ids.toml",
			},
			wantDisabledIDs: map[string]bool{},
		},
		{
			enabled:         false,
			analyzer:        "gosec",
			description:     "valid gosec analyzer ruleset",
			rulePath:        "testdata/sast-ruleset-disable-ids.toml",
			wantErr:         &NotEnabledError{},
			wantDisabledIDs: map[string]bool{},
		},
	}
	oldval, err := setFeatureEnv(GitlabFeatureCustomRulesetsSAST)
	if err != nil {
		t.Fatal(err)
	}
	defer setFeatureEnv(oldval)
	for _, test := range tests {
		if test.enabled {
			setFeatureEnv(GitlabFeatureCustomRulesetsSAST)
		}
		// Enable custom ruleset feature availability

		disabledIDs, err := DisabledIdentifiers(test.rulePath, test.analyzer)
		errEqual(t, err, test.wantErr)

		if !reflect.DeepEqual(disabledIDs, test.wantDisabledIDs) {
			t.Errorf("Wrong result. Expecting rule:\n%v\nGot:\n%v", test.wantDisabledIDs, disabledIDs)
		}
		setFeatureEnv("")
	}
}
